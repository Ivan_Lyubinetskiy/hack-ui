import styled from 'astroturf';

export const OverTableContainer = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    margin-bottom: 16px;
`;