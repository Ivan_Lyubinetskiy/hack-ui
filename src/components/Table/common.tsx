import { css } from 'astroturf';

const grey490 = '#7F828A';

export const { linkStyle, subText }= css`
    .linkStyle {
        width: 100%;
        height: 100%;
        text-decoration: none;
        cursor: pointer;
        color: inherit;
    }
    .subText {
        font-size: 13px;
        line-height: 16px;
        color: ${grey490};
    }
`;