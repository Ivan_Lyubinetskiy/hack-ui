import styled from 'astroturf';

const grey420 = '#8F939B';

const arrowsStyle = `
    position: absolute;
    display: block;
    content: '';
    width: 4px;
    height: 4px;
    right: 13px;
    border: 2px solid ${grey420};
`;
const arrowDownStyle = `
    top: 18px;
    border-left: none;
    border-bottom: none;
    transform: rotate(-45deg);
`;
const arrowUpStyle = `
    bottom: 18px;
    border-top: none;
    border-right: none;
    transform: rotate(-45deg);
`;

export const THeadButton: any = styled.button`
    position: relative;
    font-family: Roboto, Arial, sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 16px;
    text-align: left;
    width: 100%;
    height: 100%;
    padding-left: 8px;
    background-color: inherit;
    outline: none;

    &.handle-arrows {
        &::after, &::before {
            ${arrowsStyle}
        }
    
        &::after {
            ${arrowDownStyle}
        }
    
        &::before {
            ${arrowUpStyle}
        }
    }

    &.active-arrowUp {
        &::after {
            border-color: #000000;
        }
    }

    &.active-arrowDown {
        &::before {
            border-color: #000000;
        }
    }
`;