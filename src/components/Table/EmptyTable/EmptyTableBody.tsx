import styled from 'astroturf';

export const EmptyTableBody = styled.ul`
    position: relative;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    list-style: none;
    width: 100%;
`;