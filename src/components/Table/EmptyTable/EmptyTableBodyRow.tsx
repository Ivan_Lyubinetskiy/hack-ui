import styled from 'astroturf';

const grey120 = '#DDE0E6';

export const EmptyTableBodyRow = styled.ul`
    position: relative;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    width: 100%;
    min-height: 68px;
    box-shadow: inset 0 -2px 0 0 ${grey120};
    padding: 0 20px;
    box-sizing: border-box;
`;