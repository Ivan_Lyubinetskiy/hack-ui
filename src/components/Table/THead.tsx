import styled from 'astroturf';

export const THead = styled.ul`
    height: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    list-style: none;
`;
