import styled from 'astroturf';

export const PageWrapper = styled('div')`
    padding: 0 32px;
    padding-top: 16px;
    margin: 0 auto;
`;